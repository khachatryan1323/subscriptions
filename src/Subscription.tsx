import React from 'react';
import './App.css';
import { Table ,Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {useState, useEffect} from 'react';
import DetailInfo from './DetailInfo';
import CustomerId from './CustomerId';
const json = require('./data.json');


interface Subscription {
  customerId:number;
  createdAt:Date;
  amount:number;
  unsubscribeToken:string;
  lastChargeDate:Date;
  unsubscribeDate:Date;
  subscriptionType:{
    name:string;
    subscriptionTypeId:number;
  }
  subscriptionStatus:{
      name:string;
      subscriptionStatusId:number;
  }
  subscriber: {
    subscriberId:number;
    firstNmae:string;
    lastNmae:string;
    email:string;
    phone:number;
  } 
}



export default function Subscription () {
  const [subscriptions,setSubscriptions]= useState([]);
  const [modal, setModal] = useState(false);
  const [selectedRow, setSelectedRow] = useState({});

 
useEffect(() =>{
setSubscriptions(json.result)
},[]);


const ModalShow = (i?: number) => {
    if(typeof i !== 'undefined') {
       setSelectedRow(subscriptions[i]);
}
  setModal(true);   
};


const ModalClose = () => setModal(false);



function renderTableRow(subscription:any, i: number) {

  const {customerId, amount,subscriptionType, unsubscribeToken,lastChargeDate,subscriber} = subscription;

  return (
      <tr onClick={() => { ModalShow(i)}} key={customerId}>
        <th scope="row">{customerId}</th>
        <td>{subscriber.firstName}</td>
        <td>{subscriber.lastName}</td>
        <td>{amount}</td>
        <td>{lastChargeDate}</td>
        <td>{subscriptionType.name}</td>
        <td>{subscriber.email}</td>
      </tr>);
}

  return (
    <Table hover>
    <thead>
      <tr>
        <th>ID</th>
        <th>FirstName</th>
        <th>LastName</th>
        <th>Amount</th>
        <th>Date</th>
        <th>Type</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
       {subscriptions.map(renderTableRow)}
    </tbody>

      <Modal isOpen={modal}>
        <ModalHeader onClick={ModalClose}>
             {selectedRow ? <CustomerId {...selectedRow}/> : null}
        </ModalHeader>
        <ModalBody>
              {selectedRow ? <DetailInfo {...selectedRow}/> : null}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={ModalClose}>Close</Button>
        </ModalFooter>
      </Modal>
  </Table>
  );
}

