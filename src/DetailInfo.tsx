
import React from 'react';

interface DetailInfo {
    customerId:number;
    createdAt:Date;
    amount:number;
    unsubscribeToken:string;
    lastChargeDate:Date;
    unsubscribeDate:Date;
    subscriptionType:{
      name:string;
      subscriptionTypeId:number;
    }
    subscriptionStatus:{
        name:string;
        subscriptionStatusId:number;
    }
    subscriber: {
      subscriberId:number;
      firstNmae:string;
      lastNmae:string;
      email:string;
      phone:number;
    } 
  }
  

export default function DetailInfo  (subscription:any) { 
     return (
<div>
    <p>Name: {subscription.subscriber.firstName + "  " +subscription.subscriber.lastName}</p>
    <p>Subscriber ID: {subscription.subscriber.subscriberId}</p>
    <p>Amount: {subscription.amount}</p>
    <p>Email: {subscription.subscriber.email}</p>
    <p>Phone: {subscription.subscriber.phone}</p>
    <p>Created At: {subscription.createdAt}</p>
    <p>Token: {subscription.unsubscribeToken}</p>
    <p>Last Charge Date: {subscription.lastChargeDate}</p>
    <p>Unsubscribe Date: {subscription.unsubscribeDate}</p>
    <p>Subscription Type ID: {subscription.subscriptionType.subscriptionTypeId}</p>
    <p>Subscription Type: {subscription.subscriptionType.name}</p>
<    p>Subscription Status: {subscription.subscriptionStatus.name}</p>
    <p>Subscription Status Id:{subscription.subscriptionStatus.subscriptionStatusId}</p>
</div>
)};



