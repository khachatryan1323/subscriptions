import React from 'react';

interface CustomerId {
  customerId:number;
}
export default function CustomerId (subscription:any) { 
  return (
    <div>
    ID: {subscription.customerId}
  </div>
)};